import 'package:alat_musik_tuma/bermain.dart';
import 'package:alat_musik_tuma/trivia.dart';
import 'package:alat_musik_tuma/tutorial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  IndexPageState createState() => IndexPageState();
}

class IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/background.png'), fit: BoxFit.fill),
        ),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 50, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Aplikasi\nInstrumen\nTuma.',
                    style: TextStyle(
                        fontSize: 35,
                        height: 1.4,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
                const SizedBox(height: 50),
                menuLink("Bermain", const BermainPage()),
                menuLink("Trivia", const TriviaPage()),
                menuLink("Tutorial", const TutorialPage()),
              ],
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector menuLink(String namaMenu, Widget whereTo) {
    return GestureDetector(
      onTap: () {
        if (whereTo != null) {
        Navigator.push(context, CupertinoPageRoute(builder: (context) {
          return whereTo;
        }));
      }
      },
      child: Container(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        margin: const EdgeInsets.only(bottom: 25),
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            colors: [
              Color(0xFF2DD25B),
              Color(0xFF2D97D2),
            ],
            begin: FractionalOffset(0.0, 0.0),
            end: FractionalOffset(1.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              namaMenu,
              style: const TextStyle(
                fontSize: 17.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
