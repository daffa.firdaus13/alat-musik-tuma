import 'package:alat_musik_tuma/index.dart';
import 'package:alat_musik_tuma/values/color.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Alat Musik Tuma',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.green,
        primaryColor: primaryColor,
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
          primary: primaryColor,
          elevation: 0,
          minimumSize: const Size(double.infinity, 30),
          onSurface: Colors.grey,
          padding:
              const EdgeInsets.only(top: 9, bottom: 9, left: 20, right: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        )),
      ),
      home: const IndexPage(),
    );
  }
}
