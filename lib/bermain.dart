import 'package:alat_musik_tuma/values/color.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ionicons/ionicons.dart';

class BermainPage extends StatefulWidget {
  const BermainPage({Key? key}) : super(key: key);

  @override
  BermainPageState createState() => BermainPageState();
}

class BermainPageState extends State<BermainPage> {
  AudioCache audioCache = AudioCache();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: SafeArea(
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 150,
                      child: ElevatedButton.icon(
                          icon: const Icon(Icons.arrow_back_outlined,
                              color: Colors.white),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: const Text(
                            "Kembali",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                          )),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Transform.rotate(
                  angle: 0.1,
                  child: Stack(
                    children: [
                      Image.asset('assets/tuma.png',
                          height:
                              MediaQuery.of(context).size.height * 45 / 100),
                      Positioned(
                          child: GestureDetector(
                              onTap: () {
                                audioCache.play('center.mp3');
                              },
                              child: Opacity(
                                opacity: 0,
                                child: SvgPicture.asset('assets/center.svg',
                                    height: 65),
                              )),
                          left: 85,
                          top: 25),
                      Positioned(
                          child: GestureDetector(
                              onTap: () {
                                audioCache.play('kiri.mp3');
                              },
                              child: Opacity(
                                opacity: 0,
                                child: SvgPicture.asset('assets/kiri.svg',
                                    height: 65),
                              )),
                          left: 21,
                          top: 82),
                      Positioned(
                          child: GestureDetector(
                              onTap: () {
                                audioCache.play('kanan.mp3');
                              },
                              child: Opacity(
                                opacity: 0,
                                child: SvgPicture.asset('assets/kanan.svg',
                                    height: 70),
                              )),
                          left: 175,
                          top: 68),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
