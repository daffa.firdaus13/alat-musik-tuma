import 'package:alat_musik_tuma/values/color.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ionicons/ionicons.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class TutorialPage extends StatefulWidget {
  const TutorialPage({Key? key}) : super(key: key);

  @override
  TutorialPageState createState() => TutorialPageState();
}

class TutorialPageState extends State<TutorialPage> {
  final YoutubePlayerController _controller = YoutubePlayerController(
    initialVideoId: 'gEOcL-IpktI',
    flags: const YoutubePlayerFlags(
      autoPlay: true,
      mute: false,
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/background.png'), fit: BoxFit.fill),
        ),
        child: SafeArea(
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 150,
                      child: ElevatedButton.icon(
                          icon: const Icon(Icons.arrow_back_outlined,
                              color: Colors.white),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: const Text(
                            "Kembali",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                          )),
                    ),
                    const SizedBox(height: 30),
                    const Text('Tutorial',
                        style: TextStyle(
                            fontSize: 35,
                            height: 1.4,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                    const SizedBox(height: 30),
                    YoutubePlayer(
                      controller: _controller,
                      showVideoProgressIndicator: true,

                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
