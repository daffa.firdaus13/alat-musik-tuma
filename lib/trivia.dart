import 'package:alat_musik_tuma/values/color.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ionicons/ionicons.dart';

class TriviaPage extends StatefulWidget {
  const TriviaPage({Key? key}) : super(key: key);

  @override
  TriviaPageState createState() => TriviaPageState();
}

class TriviaPageState extends State<TriviaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/background.png'), fit: BoxFit.fill),
        ),
        child: SafeArea(
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 150,
                      child: ElevatedButton.icon(
                          icon: const Icon(Icons.arrow_back_outlined,
                              color: Colors.white),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: const Text(
                            "Kembali",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                          )),
                    ),
                    const SizedBox(height: 30),
                    const Text('Trivia',
                    style: TextStyle(
                        fontSize: 35,
                        height: 1.4,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
                    const SizedBox(height: 30),
                    expendableCard('Berasal dari manakah Tuma?',
                        'Tuma berasal dari daerah bla bla bla'),
                    expendableCard('Terbuat dari apakah Tuma?',
                        'Tuma berasal dari daerah bla bla bla'),
                    expendableCard('Bagaimana cara memainkan Tuma?',
                        'Tuma berasal dari daerah bla bla bla'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container expendableCard(String name, String article) {
    return Container(
        margin: const EdgeInsets.only(bottom: 15),
        padding:
            const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12.0)),
        child: ExpandablePanel(
          header: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(name,
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.black)),
          ),
          collapsed: Container(),
          expanded: Container(
            margin: const EdgeInsets.only(top: 10),
            child: Text(
              article,
              softWrap: true,
              style: const TextStyle(fontSize: 14, color: Color(0xFF7d7d7d)),
            ),
          ),
        ));
  }
}
